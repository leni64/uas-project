
<!DOCTYPE html>
<html lang="en">
  <head>
    <style type="text/css">
        label{
            display: inline-block;
            width: 200px;
        }
    
        </style>
    <!-- Required meta tags -->
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text"> Selamat Datang</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body wrapper">
      
            <div class="container" align="center" style="padding-top: 100px;">
              @if(session()->has('message'))
    
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">
              
            </button>
            {{session()->get('message')}}
          </div>
          @endif
    
                <form action="{{url('add_pasien')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div style="padding: 15px;">
                    <label for="">Nama Pasien</label>
                    <input type="text" style="color:black; " name="nama" placeholder="Masukkan nama!">
                </div>

                <div style="padding: 15px;">
                    <label for="">Tanggal lahir</label>
                    <input type="date" style="color:black; width=:200px" name="date" placeholder="tanggal lahir!">
                </div>

                <div style="padding: 15px;">
                    <label for="">Alamat</label>
                    <input type="text" style="color:black; " name="alamat" placeholder="Masukkan alamat!">
                </div>

                <div style="padding: 15px;">
                    <label for="">Pekerjaan</label>
                    <input type="text" style="color:black; width=:200px" name="work" placeholder="Masukkan pekerjaan!">
                </div>

                <div style="padding: 15px;">
                    <label for="">Keluhan</label>
                    <input type="text" style="color:black; " name="keluhan" placeholder="Masukkan Keluhan!">
                </div>

                <div style="padding: 15px;">
                    <label for="">Diagnosa</label>
                    <input type="text" style="color:black; " name="diagnosa" placeholder="Masukkan Diagnosa!">
                </div>

                    <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
                  </form>
    
                    
    
    
    
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>