
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
<base href="/public">

<style type="text/css">
    label{
        display: inline-block;
        width: 200px;
    }
</style>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text"> Selamat Datang</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        <!-- partial -->
     
        <div class="container-fluid page-body wrapper">

            <div class="container" align="center" style="padding-top:100px;">
                @if(session()->has('message'))

            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert">
                x
              </button>
              {{session()->get('message')}}
            </div>
            @endif

                <form action="{{url('editpasien',$data->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div style="padding: 15px;">
                        <label for="">Nama Pasien</label>
                        <input type="text" name="nama" style="color: black" value="{{$data->nama}}">
                    </div>

                    <div style="padding: 15px;">
                        <label for="">Tanggal Lahir</label>
                        <input type="date" name="date" style="color: black" value="{{$data->date}}">
                    </div>
                    <div style="padding: 15px;">
                        <label for="">Alamat</label>
                        <input type="text" name="alamat" style="color: black" value="{{$data->alamat}}">
                    </div>
                    <div style="padding: 15px;">
                        <label for="">Pekerjaan</label>
                        <input type="text" name="work" style="color: black" value="{{$data->work}}">
                    </div>

                    <div style="padding: 15px;">
                        <label for="">Keluhan</label>
                        <input type="text" name="keluhan" style="color: black" value="{{$data->keluhan}}">
                    </div>
                    <div style="padding: 15px;">
                        <label for="">Diagnosa</label>
                        <input type="text" name="diagnosa" style="color: black" value="{{$data->diagnosa}}">
                    </div>
                    
                    <div style="padding: 15px;">
                        
                        <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>