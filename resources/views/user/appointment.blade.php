<div class="page-section" style="background-color: rgb(173, 233, 213)">
  <div class="container">
    <h1 class="text-center wow fadeInUp">JANJI TEMU</h1>

    <form class="main-form" action="{{url('appointment')}}" method="POST">
      @csrf

      <div class="row mt-5 ">
        <div class="col-12 col-sm-6 py-2 wow fadeInLeft">
          <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
        </div>
        <div class="col-12 col-sm-6 py-2 wow fadeInRight">
          <input type="text" name="email" class="form-control" placeholder="Email">
        </div>
        <div class="col-12 col-sm-6 py-2 wow fadeInLeft" data-wow-delay="300ms">
          <input type="date" name="date" class="form-control">
        </div>
        <div class="col-12 col-sm-6 py-2 wow fadeInRight" data-wow-delay="300ms">
          <select name="doctor" id="departement" class="custom-select">
              <option value="">Pilih Dokter</option>

              @foreach ($doctor as $doctors )

            <option value=" {{$doctors->name }}"> {{$doctors->name }} Spesialis {{$doctors->specialty}}</option>

            @endforeach
          </select>
        </div>
        <div class="col-12 py-2 wow fadeInUp" data-wow-delay="300ms">
          <input type="text" name="number" class="form-control" placeholder="No.Hp">
        </div>
        <div class="col-12 py-2 wow fadeInUp" data-wow-delay="300ms">
          <textarea name="message" id="message" class="form-control" rows="6" placeholder="Pesan/Keluhan"></textarea>
        </div>
      </div>

      <button type="submit" class="btn btn-info mt-3 wow zoomIn" style="background-color: rgb(43, 97, 97);">Kirim</button>
    </form>
  </div>
</div>
