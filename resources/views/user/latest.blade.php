<div class="page-section bg-light">
    <div class="container">
      <h1 class="text-center wow fadeInUp">Fasilitas</h1>
      <a href="#latest"></a>
      <div class="row mt-5">
        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="" class="post-thumb">
                <img src="../assets/img/blog/ugd.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="blog-details.html" class="post-thumb">
                <img src="../assets/img/blog/1.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="blog-details.html" class="post-thumb">
                <img src="../assets/img/blog/3.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="blog-details.html" class="post-thumb">
                <img src="../assets/img/blog/igd.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="blog-details.html" class="post-thumb">
                <img src="../assets/img/blog/rs.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 py-2 wow zoomIn">
          <div class="card-blog">
            <div class="header">
              <div class="post-category">
                <a href="#">RS Palang Biru</a>
              </div>
              <a href="blog-details.html" class="post-thumb">
                <img src="../assets/img/blog/2.jpg" alt="">
              </a>
            </div>
            <div class="body">
              <h5 class="post-title"><a href=""></a></h5>
              <div class="site-info">
                <div class="avatar mr-2">
                  <div class="avatar-img">
                    <img src="../assets/img/blog/logo.jpg" alt="">
                  </div>
                  <span>RUMAH SAKIT PALANG BIRU KUTOARJO</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 text-center mt-4 wow zoomIn">
          <a href="blog.html" class="btn btn-primary">Read More</a>
        </div>

      </div>
    </div>
  </div> 