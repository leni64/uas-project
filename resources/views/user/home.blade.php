<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="copyright" content="MACode ID, https://macodeid.com/">

  <title>Hospital</title>

  <link rel="stylesheet" href="../assets/css/maicons.css">

  <link rel="stylesheet" href="../assets/css/bootstrap.css">

  <link rel="stylesheet" href="../assets/vendor/owl-carousel/css/owl.carousel.css">

  <link rel="stylesheet" href="../assets/vendor/animate/animate.css">

  <link rel="stylesheet" href="../assets/css/theme.css">
</head>
<body>

  <!-- Back to top button -->
  <div class="back-to-top"></div>

  <header>
    <div class="topbar">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 text-sm">
            <div class="site-info">
              <a href="#"><span class="mai-call text-primary"></span> +62 898 4574 129</a>
              <span class="divider">|</span>
              <a href="#"><span class="mai-mail text-primary"></span> leniwfm@gmail.com</a>
            </div>
          </div>
          <div class="col-sm-4 text-right text-sm">
            <div class="social-mini-button">
              <a href="#"><span class="mai-logo-facebook-f"></span></a>
              <a href="#"><span class="mai-logo-twitter"></span></a>
              <a href="#"><span class="mai-logo-dribbble"></span></a>
              <a href="#"><span class="mai-logo-instagram"></span></a>
            </div>
          </div>
        </div> <!-- .row -->
      </div> <!-- .container -->
    </div> <!-- .topbar -->

    <nav class="navbar navbar-expand-lg navbar-light shadow-sm">
      <div class="container">
        <a class="navbar-brand" href="#"><span class="text-primary">RUMAH SAKIT</span>-PALANG BIRU</a>

        

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupport" aria-controls="navbarSupport" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupport">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.html">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.html">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="doctors.html">Profil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#latest">Fasilitas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.html">Kontak</a>
            </li>
            
            @if(Route::has('login'))

            @auth

            <li class="nav-item">
              <a class="nav-link" style="background-color:aquamarine;" href="{{url('myappointment')}}">My Appointment</a>
            </li>

            <x-app-layout>
            </x-app-layout>

            @else
           
            <li class="nav-item">
              <a class="btn btn-primary ml-lg-3" href="{{ route('login') }}">Login</a>
            </li>

            <li class="nav-item">
                <a class="btn btn-primary ml-lg-3" href="{{ route('register') }}">Register</a>
              </li>

              @endauth
              @endif

          </ul>
        </div> <!-- .navbar-collapse -->
      </div> <!-- .container -->
    </nav>
  </header>
  @if(session()->has('message'))

  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">
      x
    </button>
    {{session()->get('message')}}
  </div>
  @endif

  <div class="page-hero bg-image overlay-dark" style="background-image: url(../assets/img/palangbiru.jpg);">
    <div class="hero-section">
      <div class="container text-center wow zoomIn">
        <span class="subhead">Ayo Buat Hidup Kita Lebih Bahagia</span>
        <h1 class="display-4">Dengan Hidup Sehat!</h1>
        <a href="#" class="btn btn-primary">Ayo Konsultasi!</a>
      </div>
    </div>
  </div>


  <div class="bg-light">
    <div class="page-section py-3 mt-md-n5 custom-index">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-4 py-3 py-md-0">
            <div class="card-service wow fadeInUp">
              <div class="circle-shape bg-secondary text-white">
                <span class="mai-chatbubbles-outline"></span>
              </div>
              <p><span>Chat</span> dengan Dokter</p>
            </div>
          </div>
          <div class="col-md-4 py-3 py-md-0">
            <div class="card-service wow fadeInUp">
              <div class="circle-shape bg-primary text-white">
                <span class="mai-shield-checkmark"></span>
              </div>
              <p><span>Perlindungan</span>-Kesehatan</p>
            </div>
          </div>
          <div class="col-md-4 py-3 py-md-0">
            <div class="card-service wow fadeInUp">
              <div class="circle-shape bg-accent text-white">
                <span class="mai-basket"></span>
              </div>
              <p><span>Farmasi</span>-Palang Biru</p>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- .page-section -->

    <div class="page-section pb-0">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 py-3 wow fadeInUp">
            <h1>Selamat Datang<br> <span class="text-primary">di Rumah Sakit Palang Biru</span> </h1>
            <p class="text-grey mb-4">Rumah Sakit Palang Biru kutoarjo adalah rumah sakit swasta umum yang berkomitmen memberikan pelayanan kesehatan berkualitas dari staf berdedikasi dan profesional, didukung teknologi terkini dan standar fasilitas kesehatan tinggi.</p>
            <a href="about.html" class="btn btn-primary">Selengkapnya</a>
          </div>
          <div class="col-lg-6 wow fadeInRight" data-wow-delay="400ms">
            <div class="img-place custom-img-1">
              <img src="../assets/img/bg-doctor.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div> <!-- .bg-light -->
  </div> <!-- .bg-light -->

  @include('user.doctor')

  @include('user.latest')
  
  @include('user.appointment')

  
  <footer class="page-footer">
    <div class="container">
      <div class="row px-md-3">
        <div class="col-sm-6 col-lg-3 py-3">
          <h5>Lainnya</h5>
          <ul class="footer-menu">
            <li><a href="#">Agenda</a></li>
            <li><a href="#">Pengumuman</a></li>
            <li><a href="#">Berita</a></li>
            <li><a href="#">Karier</a></li>
            <li><a href="#">Buku Tamu</a></li>
          </ul>
        </div>
        <div class="col-sm-6 col-lg-3 py-3">
          <h5>Informasi</h5>
          <ul class="footer-menu">
            <li><a href="#">Pendaftaran Online</a></li>
            <li><a href="#">Jadwal Dokter</a></li>
            <li><a href="#">Info Harian Ketersediaan Kamar Rawat Inap</a></li>
            <li><a href="#">Info Harian Pelayanan Rawat Jalan</a></li>
          </ul>
        </div>
        <div class="col-sm-6 col-lg-3 py-3">
          <h5>Pelayanan Medik</h5>
          <ul class="footer-menu">
            <li><a href="#">Poliklinik</a></li>
            <li><a href="#">Instalasi Gawat Darurat</a></li>
            <li><a href="#">Kamar Bedah</a></li>
            <li><a href="#">Hermodialisa (Cuci Darah)</a></li>
            <li><a href="#">Medical Check Up</a></li>
          </ul>
        </div>
        <div class="col-sm-6 col-lg-3 py-3">
          <h5>Kontak</h5>
          <p class="footer-link mt-2">(0275) 641425, 641650</p>
          <a href="#" class="footer-link">(0275) 642560</a>
          <a href="#" class="footer-link">RS Palang Biru Kutoarjo, Purworejo</a>

          <h5 class="mt-3">Social Media</h5>
          <div class="footer-sosmed mt-3">
            <a href="#" target="_blank"><span class="mai-logo-facebook-f"></span></a>
            <a href="#" target="_blank"><span class="mai-logo-twitter"></span></a>
            <a href="#" target="_blank"><span class="mai-logo-google-plus-g"></span></a>
            <a href="#" target="_blank"><span class="mai-logo-instagram"></span></a>
            <a href="#" target="_blank"><span class="mai-logo-linkedin"></span></a>
          </div>
        </div>
      </div>

      <hr>

    </div>
  </footer>

<script src="../assets/js/jquery-3.5.1.min.js"></script>

<script src="../assets/js/bootstrap.bundle.min.js"></script>

<script src="../assets/vendor/owl-carousel/js/owl.carousel.min.js"></script>

<script src="../assets/vendor/wow/wow.min.js"></script>

<script src="../assets/js/theme.js"></script>
  
</body>
</html>