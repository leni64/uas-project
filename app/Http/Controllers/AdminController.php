<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Pasien;
use App\Models\Appointment;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function addview()
    {
        //
        return view('admin.add_doctor');
    }
    public function upload(Request $request)
    {
        $doctor=new Doctor;
        $image=$request->file;
        $imagename=time().'.'.$image->getClientOriginalExtension();

        $request->file->move('doctorimage',$imagename);
        $doctor->image=$imagename;

        $doctor->name=$request->name;
        $doctor->phone=$request->number;
        $doctor->room=$request->room;
        $doctor->specialty=$request->specialty;

        $doctor->save();
        
        return redirect()->back()->with('message','Data Dokter Berhasil Ditambah');
        ;
    }

    public function showappointment()
    {
        $data=Appointment::all();
        return view('admin.showappointment',compact('data'));
    }

    public function approved($id)
    {
        $data=Appointment::find($id);
        $data->status='disetujui';
        $data->save();
        return redirect()->back();
    }
    public function canceled($id)
    {
        $data=Appointment::find($id);
        $data->status='ditunda';
        $data->save();
        return redirect()->back();
    }

    public function showdoctor()
    {

        $data=Doctor::all();
        return view('admin.showdoctor',compact('data'));
    }

    public function deletedoctor($id)
    {
        $data=Doctor::find($id);
        $data->delete();
        return redirect()->back();
    }
    public function updatedoctor($id)
    {
        $data=Doctor::find($id);
        return view('admin.update_doctor',compact('data'));

    }

    public function editdoctor(Request $request ,$id)
    {
        $doctor=Doctor::find($id);
        $doctor->name=$request->name;
        $doctor->phone=$request->phone;
        $doctor->specialty=$request->specialty;
        $doctor->room=$request->room;
       
        $image=$request->file;
        if($image)
        {
            $imagename=time().'.'.$image->getClientOriginalExtension();
        $request->file->move('doctorimage',$imagename);
        $doctor->image=$imagename;
        }
        
        $doctor->save();
        return redirect()->back()->with('message','Data Dokter Berhasil Diupdate');

    }
    public function addpasien()
    {
        return view('admin.addpasien');
    }
    public function input(Request $request)
    {
        $pasien=new Pasien;

        $pasien->nama=$request->nama;
        $pasien->date=$request->date;
        $pasien->alamat=$request->alamat;
        $pasien->work=$request->work;
        $pasien->keluhan=$request->keluhan;
        $pasien->diagnosa=$request->diagnosa;
        
        $pasien->save();
        
        return redirect()->back()->with('message','Data Pasien Berhasil Ditambah');
        ;
    }
    public function showpasien()
    {

        $data=Pasien::all();
        return view('admin.showpasien',compact('data'));
    }
    public function deletepasien($id)
    {
        $data=Pasien::find($id);
        $data->delete();
        return redirect()->back();
    }
    public function updatepasien($id)
    {
        $data=Pasien::find($id);
        return view('admin.update_pasien',compact('data'));

    }
    public function editpasien(Request $request ,$id)
    {
        $pasien=Pasien::find($id);
        $pasien->nama=$request->nama;
        $pasien->date=$request->date;
        $pasien->alamat=$request->alamat;
        $pasien->work=$request->work;
        $pasien->keluhan=$request->keluhan;
        $pasien->diagnosa=$request->diagnosa;
        
        
        $pasien->save();
        return redirect()->back()->with('message','Data Pasien Berhasil Diupdate');

    }


}

